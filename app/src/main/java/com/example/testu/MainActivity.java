package com.example.testu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {
  Button btn1 ,btn2 ;
  EditText ed1,ed2 ;
  CheckBox ck1 ,ck2 ;
  RadioButton ra1, ra2,ra3 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn1 = (Button) findViewById(R.id.button);
        btn2 = (Button) findViewById(R.id.button2);
        ed1=(EditText) findViewById(R.id.editTextTextPersonName2);
        ed2=(EditText) findViewById(R.id.editTextTextPersonName);
        ck1=(CheckBox) findViewById(R.id.checkBox);
        ck2=(CheckBox) findViewById(R.id.checkBox2);
        ra1=(RadioButton) findViewById(R.id.radioButton);
        ra2=(RadioButton) findViewById(R.id.radioButton2);
        ra3=(RadioButton) findViewById(R.id.radioButton3);
    }
}