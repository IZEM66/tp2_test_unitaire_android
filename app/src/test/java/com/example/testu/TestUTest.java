package com.example.testu;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestUTest {

private  TestU u = new TestU() ;

    @Test
    public void add() throws Exception {
        assertEquals(10,u.add(5,5));
    }
    @Test
    public void testKO() throws Exception {
        assertEquals(5,u.add(5,5));
    }
    @Test
    public void testTDD() throws Exception {
        TestU My = new TestU();
       //TDD
        assertEquals(10,My.sub(15,5));
        // si V2 > V1 --> 0
        assertEquals(0,My.sub(5,15));
        // si V2 = V1 --> 0
        assertEquals(0,My.sub(5,5));


    }


}